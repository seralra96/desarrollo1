var turno="X";
var turnoz= 0


let miBoton;
let formulario;
let i;
formulario=document.getElementById("tablero");

formulario=document.getElementById("tablero");
for(let i=1;i<10;i++){
    miBoton =document.createElement("input");
    miBoton.setAttribute("type", "button");
    miBoton.setAttribute("class", "casilla");
    miBoton.setAttribute("value", " " )
    miBoton.setAttribute("id", "boton"+i)
    miBoton.setAttribute("onclick", "realizarJugada(this)");
        
    formulario.appendChild(miBoton);
    if (i%3==0){
        let salto =document.createElement("br");
        formulario.appendChild(salto)
        }

}

function realizarJugada(boton){

    boton.value = turno;
    boton.disabled = true;
    if(turno=="X"){
        turno="O";
        document.getElementById('turnotx').innerHTML = "Turno: "+ turno;
    }else{
        turno="X";
        document.getElementById('turnotx').innerHTML = "Turno: "+ turno;
    }
    if(turnoz > 3)
        comprobarGanador(boton);
    turnoz++;

}

function reiniciar()
{
    turno = "X";
    turnoz = 0;
    document.getElementById('turnotx').innerHTML = "Turno: " + turno;
    
    for (var i = 1; i <= 9; i++) 
    {
        document.getElementById('boton'+i).value = " ";
        document.getElementById('boton'+i).disabled = false;
    }
    turnoAleatorio()
}


function comprobarGanador(boton)
{
    var cas1 = document.getElementById('boton1').value;
    var cas2 = document.getElementById('boton2').value;
    var cas3 = document.getElementById('boton3').value;
    var cas4 = document.getElementById('boton4').value;
    var cas5 = document.getElementById('boton5').value;
    var cas6 = document.getElementById('boton6').value;
    var cas7 = document.getElementById('boton7').value;
    var cas8 = document.getElementById('boton8').value;
    var cas9 = document.getElementById('boton9').value;

    if((cas1 == boton.value && cas2 == boton.value && cas3 == boton.value) || 
       (cas4 == boton.value && cas5 == boton.value && cas6 == boton.value) || 
       (cas7 == boton.value && cas8 == boton.value && cas9 == boton.value))
			{
                //reiniciar();
                deshabilitarBotones();
                alert("El ganador es: " + boton.value);
            }
    else if((cas1 == boton.value && cas4 == boton.value && cas7 == boton.value) || 
            (cas2 == boton.value && cas5 == boton.value && cas8 == boton.value) || 
            (cas3 == boton.value && cas6 == boton.value && cas9 == boton.value))
			{

                //reiniciar();
                deshabilitarBotones();
				alert("El ganador es: " + boton.value);
            }
    else if((cas1 == boton.value && cas5 == boton.value && cas9 == boton.value) || 
            (cas3 == boton.value && cas5 == boton.value && cas7 == boton.value))
			{
                //reiniciar();
                deshabilitarBotones();
				alert("El ganador es: " + boton.value);
            }

}

function deshabilitarBotones(){

    for (var i = 1; i <= 9; i++) 
    {
        document.getElementById('boton'+i).disabled = true;
    }
    document.getElementById('turnotx').innerHTML = "Pulsa reiniciar para iniciar un nuevo juego";
}
function turnoAleatorio(){
    
    var turno_aleatorio = Math.random() >= 0.5;
    if (turno_aleatorio)
    {turno="O";}else{
        turno="X";
    }
    document.getElementById('turnotx').innerHTML = "Turno: "+ turno;
}